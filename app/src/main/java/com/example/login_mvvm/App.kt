package com.example.login_mvvm

import android.app.Application
import com.example.login_mvvm.data.repository.Api
import com.example.login_mvvm.data.repository.LoginRepository
import com.example.login_mvvm.utils.LocalPreferences
import java.util.logging.Handler

class App: Application() {

    companion object{
        lateinit var repository: LoginRepository

    }

    override fun onCreate() {
        super.onCreate()
        repository = LoginRepository()

        repository.getToken{token, error ->
            if (token != null) {
                LocalPreferences(this).access_token = token
            }
        }


    }
}