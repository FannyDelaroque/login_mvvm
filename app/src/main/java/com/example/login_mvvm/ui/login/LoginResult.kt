package com.example.login_mvvm.ui.login

/**
 * Authentication result : success (user details) or error message.
 */
interface LoginResult{
        fun onSuccess(message:String)
        fun onFailure(message: String)
        fun onError(message: String)
        fun onPreprare()
}


