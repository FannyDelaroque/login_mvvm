package com.example.login_mvvm.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.example.login_mvvm.data.repository.LoginRepository
import com.example.login_mvvm.ui.login.LoginActivity

class Splashscreen: AppCompatActivity() {

    /**
     * Application constituée de deux écrans:
     * 1. splashscreen
     * 2. écran de login
     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

}