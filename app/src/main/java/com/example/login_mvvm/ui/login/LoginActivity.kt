package com.example.login_mvvm.ui.login

import android.content.Context
import androidx.lifecycle.Observer
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider

import com.example.login_mvvm.R
import com.example.login_mvvm.utils.LocalPreferences
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), TextWatcher {

    private lateinit var loginViewModel: LoginViewModel
    private var isValid: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        /**
         * permet de cacher le clavier au lancement de l'activité
         */
        hideKeyboard()

        /**
         * instanciation du viewmodel
         */
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        /**
         * abonnement à l'état: onPrepare, onSuccess, onError, onFailure
         */
        loginViewModel.state.observe(this, Observer { state -> updateUI(state) })

        /**
         * TextWatcher permet de griser le bouton tant que username et password ne sont pas remplis
         */
        input_username.addTextChangedListener(this)
        input_password.addTextChangedListener(this)


        }

    private fun updateUI(state: LoginState?) {
        if (state != null) {
            return when (state) {
                is LoginStateSuccess -> displaySuccessMessage(state.message)
                is LoginStateError-> displayErrorMessage(state.message)
                is LoginStateFailure -> displayFailureMessage(state.message)
                is LoginStateOnPrepare -> displayProgressBar()
                else -> {Toast.makeText(this, state.message, Toast.LENGTH_SHORT).show()}
            }
        }
    }

    private fun displaySuccessMessage(message: String) {
        progressBar.visibility = View.GONE
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun displayProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun displayFailureMessage(message: String) {
        progressBar.visibility = View.GONE
        message_error.visibility = View.VISIBLE
        message_error.text = message
    }


    private fun displayErrorMessage(message: String) {
        progressBar.visibility = View.GONE
        message_error.visibility = View.VISIBLE
        message_error.text = message
    }

    fun onLoginButtonClicked(view: View){
        val username = input_username.text.toString()
        val password = input_password.text.toString()
        val token = LocalPreferences(this).access_token
        if (token != null) {
            loginViewModel.login(username,password,token)
        }
    }

    override fun afterTextChanged(s: Editable?) {
        message_error.visibility = View.GONE
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        message_error.visibility = View.GONE
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        message_error.visibility = View.GONE
        if(input_username.text.isNotEmpty() && input_password.text.isNotEmpty()){
            isValid = true
        }
        if(isValid){
            btn_login.isEnabled = true
            btn_login.setBackgroundColor( ContextCompat.getColor(this, R.color.colorPrimary))
            btn_login.setTextColor( ContextCompat.getColor(this, R.color.white))
        }

    }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }


}



