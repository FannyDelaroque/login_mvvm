package com.example.login_mvvm.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.login_mvvm.App

sealed class LoginState(val message: String ="")
class LoginStateFailure(message: String) : LoginState(message)
class LoginStateError(message: String) : LoginState(message)
class LoginStateSuccess(message: String) : LoginState(message)
class LoginStateOnPrepare(): LoginState()

class LoginViewModel() : ViewModel(), LoginResult {



    val loginResult:LoginResult = this

    private val _state = MutableLiveData<LoginState>()
    val state : LiveData<LoginState> = _state

    fun login(username: String, password: String, token: String) {
        // can be launched in a separate asynchronous job
        App.repository.checkAuthentication(username, password,token, loginResult)


    }


    override fun onSuccess(message: String) {
        _state.value = LoginStateSuccess("Success")
    }

    override fun onError(message: String) {

        _state.value = LoginStateError("Invalid username and/or password")
    }

    override fun onPreprare() {
        _state.value = LoginStateOnPrepare()
    }

    override fun onFailure(message: String) {
     _state.value = LoginStateFailure("$message\nCheck your internet connection")
    }




}
