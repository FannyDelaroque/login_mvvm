package com.example.login_mvvm.data.repository

import com.example.login_mvvm.App
import com.example.login_mvvm.data.model.Token
import com.example.login_mvvm.data.model.User
import com.example.login_mvvm.utils.LocalPreferences
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface Api {

    /**
     * login
     */

    @Headers(
        "Accept: application/json",
        "Content-Type: application/json; charset=utf-8"
    )
    @POST("authentication/token/validate_with_login?")
    fun postUserData(@Body user: User,
                     @Query("api_key") key: String): Call<ResponseBody>

    /**
     * obtention d'un token
     */

    @GET("authentication/token/new?")
    fun getToken(@Query("api_key") key: String):Call<Token>
}