package com.example.login_mvvm.data.repository

import android.util.Log
import com.example.login_mvvm.data.model.Token
import com.example.login_mvvm.data.model.User
import com.example.login_mvvm.ui.login.LoginResult
import com.example.login_mvvm.utils.ApiError
import com.example.login_mvvm.utils.ErrorUtil
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

private const val API_KEY = "f6ca13cd71f090bdd508521c36bf0b63"

class LoginRepository() {

   val service = RetrofitBuilder.buildService(Api::class.java)


    fun checkAuthentication(userName:String,password:String, token: String, loginResult: LoginResult){
        var user =User(userName,password,token)
        loginResult.onPreprare()
        service.postUserData(user,API_KEY).enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("LoginRepository", t.message)
                loginResult.onFailure(t.message.toString())

            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.isSuccessful){
                    loginResult.onSuccess(response.message())
                } else{
                    loginResult.onError(response.message())
                }
            }
        })
    }

    fun getToken(resultHandler:(token:String, error:String?)->Unit){
        service.getToken(API_KEY).enqueue(object :Callback<Token>{
            override fun onFailure(call: Call<Token>, t: Throwable) {
                Log.d("LoginRepository", t.message)

            }

            override fun onResponse(call: Call<Token>, response: Response<Token>) {
                if(response.isSuccessful){
                    val token = response.body()?.request_token
                    resultHandler(token!!,null)
                }
                Log.d("RepositoryOnResponse", response.body().toString())
            }
        })
    }


}
