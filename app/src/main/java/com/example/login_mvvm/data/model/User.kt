package com.example.login_mvvm.data.model

import com.google.gson.annotations.SerializedName

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class User(
        @SerializedName("username")
        val userName: String,
        @SerializedName("password")
        val password:String,
        @SerializedName("request_token")
        val request_token: String

)
