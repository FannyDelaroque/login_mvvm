package com.example.login_mvvm.data.model

import com.google.gson.annotations.SerializedName

data class Token(
    @SerializedName("request_token")
    val request_token: String
)