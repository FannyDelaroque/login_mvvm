package com.example.login_mvvm.utils

import android.content.Context
import android.preference.PreferenceManager
import com.example.login_mvvm.data.model.Token

class LocalPreferences (context: Context) {
    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        var access_token: String?
        get() {
            return sharedPreferences.getString("request_token", null)
        }
        set(value) {
            sharedPreferences.edit().putString("request_token", value).apply()
        }


}