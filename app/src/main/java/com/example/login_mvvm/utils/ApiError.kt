package com.example.login_mvvm.utils

class ApiError() {
    var statusCode:Int =0
    var message:String =""
   /* private val statusCode:Int = 0
    private lateinit var message:String
    fun status():Int {
        return statusCode
    }
    fun message():String {
        return message
    }*/

    constructor(statusCode: Int, message: String):this(){
        this.statusCode= statusCode
        this.message = message
    }
}