package com.example.login_mvvm.utils


import com.example.login_mvvm.data.repository.Api
import com.example.login_mvvm.data.repository.RetrofitBuilder
import retrofit2.Response
import java.io.IOException

object ErrorUtil {
    fun parseError(response: Response<*>):ApiError {
        val converter = RetrofitBuilder.retrofit
            .responseBodyConverter<ApiError>(ApiError::class.java, arrayOfNulls<Annotation>(0))
        val error:ApiError
        try
        {
            error = converter.convert(response.errorBody())!!
        }
        catch (e: IOException) {
            return ApiError()
        }
        return error
    }
}